# README #

### What is this? ###

This is a splunk app with a few scripts that you might find useful for your dashboards.

Check out my site to learn more about it:
* https://www.gabrielvasseur.com/post/easy-yet-powerful-submit-buttons-in-your-simple-xml-dashboards


### How do I get set up? ###

Just install it how you would any other splunk app.

### So where is it? ###

This app is now on splunkbase: [GV-Utils](https://splunkbase.splunk.com/app/6269)